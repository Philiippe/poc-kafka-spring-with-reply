package com.demo.controllers;

import org.springframework.web.bind.annotation.*;

/**
 * Deuxième API que l'on veut consulter pour obtenir des données
 * (elle n'est pas utile en elle même mais on lui a donné la forme
 * d'une API pour la simuler)
 */
@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {

    @GetMapping(value = "/publish")
    public void sendMessageToKafkaTopic(@RequestParam("message") String message) {
        System.out.println("API de manipulation de données");
    }
}
