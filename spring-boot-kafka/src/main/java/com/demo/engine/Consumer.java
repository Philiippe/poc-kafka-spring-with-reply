package com.demo.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class Consumer {

    /**
     * Consommateur qui va récupérer les données et les manipuler avant de les renvoyer
     * @param message données à manipuler
     * @return avec l'annotation SendTo, on va pouvoir retourner une valeur via requestReplyTopic
     * @throws IOException
     */
    @KafkaListener(topics = "sum-topic", groupId = "helloworld")
    @SendTo
    public String consume(String message) throws IOException {
        try {
            message = Integer.toString(Integer.parseInt(message) + 1);
        } catch (Exception e) {
            message = "ERROR - Entrez un entier";
        }
        return message;
    }
}
