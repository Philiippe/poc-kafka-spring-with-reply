1. Ouvrir le projet RequestReplyKafka et spring-boot-with-kafka sur IntelliJ
(Si besoin utiliser la notice sur Spring pour voir comment ouvrir ces projets).

2. Lancez ensuite ces deux projets en parallèle

3. Ouvrez Postman et lancez une requête POST sur localhost:8080/sum avec un paramètre 
"message" -> "localhost:8080/sum?message=7" par exemple